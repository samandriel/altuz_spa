import Vue from 'vue'
import Vuex from 'vuex'
import * as getters from './getters'
import * as mutations from './mutations'
import * as actions from './actions'
Vue.use(Vuex)

export const store = new Vuex.Store({
  state: {
    locale: localStorage.getItem('locale') !== null ? JSON.parse(localStorage.getItem('locale')) : {name: 'english', code: 'en', flagIcon: 'uk.png'}
  },
  getters,
  mutations,
  actions
})
