export const locale = (state, payload) => {
  localStorage.setItem('locale', JSON.stringify(payload))
  state.locale = payload
}
