import Vue from 'vue'
import I18n from 'vue-i18n'

Vue.use(I18n)

function init () {
  if (localStorage.getItem('locale') !== null) {
    let localeState = JSON.parse(localStorage.getItem('locale'))
    return localeState.code
  }
  return 'en'
}

const i18n = new I18n({
  locale: init(),
  fallbackLocale: 'en'
})

export default i18n
