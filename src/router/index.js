import Vue from 'vue'
import Router from 'vue-router'
import Main from '@/components/Main'
import Contact from '@/components/Contact'
import Works from '@/components/Work'
import About from '@/components/About'
import WebDesign from '@/components/works/WebDesign'
import WebApp from '@/components/works/WebApp'
import MobileApp from '@/components/works/MobileApp'
import GraphicDesign from '@/components/works/GraphicDesign'
// import LandingPageLayout from '@/components/layouts/LandingPage'
Vue.use(Router)

// Router.beforeEach((to, form, next) => {
//   if (to.path === '/') {

//   }
// })

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      component: Main
    },
    {
      path: '/:locale',
      alias: '/',
      name: 'Homepage',
      component: Main
    },
    {
      path: '/:locale/about',
      name: 'About',
      component: About
    },
    {
      path: '/:locale/contact',
      name: 'Contact',
      component: Contact
    },
    {
      path: '/:locale/works',
      // name: 'Works',
      component: Works,
      children: [
        {
          path: '',
          name: 'WorksDefault',
          component: WebDesign
        },
        {
          path: 'web-design',
          alias: '',
          name: 'WebDesign',
          component: WebDesign
        },
        {
          path: 'web-apps',
          name: 'WebApp',
          component: WebApp
        },
        {
          path: 'mobile-apps',
          name: 'MobileApp',
          component: MobileApp
        },
        {
          path: 'graphic-design',
          name: 'GraphicDesign',
          component: GraphicDesign
        }
      ]
    }
    // {
    //   path: '/en',
    //   name: 'en_Homepage',
    //   component: Main
    // },
    // {
    //   path: '/en/about',
    //   name: 'en_About',
    //   component: About
    // },
    // {
    //   path: '/en/contact',
    //   name: 'en_Contact',
    //   component: Contact
    // },
    // {
    //   path: '/en/works',
    //   // name: 'Works',
    //   component: Works,
    //   children: [
    //     {
    //       path: '',
    //       name: 'en_WorksDefault',
    //       component: WebDesign
    //     },
    //     {
    //       path: 'web-design',
    //       alias: '',
    //       name: 'en_WebDesign',
    //       component: WebDesign
    //     },
    //     {
    //       path: 'web-apps',
    //       name: 'en_WebApp',
    //       component: WebApp
    //     },
    //     {
    //       path: 'mobile-apps',
    //       name: 'en_MobileApp',
    //       component: MobileApp
    //     },
    //     {
    //       path: 'graphic-design',
    //       name: 'en_GraphicDesign',
    //       component: GraphicDesign
    //     }
    //   ]
    // },
    // {
    //   path: '/th',
    //   name: 'th_Homepage',
    //   component: Main
    // },
    // {
    //   path: '/th/about',
    //   name: 'th_About',
    //   component: About
    // },
    // {
    //   path: '/th/contact',
    //   name: 'th_Contact',
    //   component: Contact
    // },
    // {
    //   path: '/th/works',
    //   // name: 'Works',
    //   component: Works,
    //   children: [
    //     {
    //       path: '',
    //       name: 'th_WorksDefault',
    //       component: WebDesign
    //     },
    //     {
    //       path: 'web-design',
    //       alias: '',
    //       name: 'th_WebDesign',
    //       component: WebDesign
    //     },
    //     {
    //       path: 'web-apps',
    //       name: 'th_WebApp',
    //       component: WebApp
    //     },
    //     {
    //       path: 'mobile-apps',
    //       name: 'th_MobileApp',
    //       component: MobileApp
    //     },
    //     {
    //       path: 'graphic-design',
    //       name: 'th_GraphicDesign',
    //       component: GraphicDesign
    //     }
    //   ]
    // }
  ]
})
