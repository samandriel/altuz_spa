/* eslint-disable camelcase */
/* eslint-disable no-undef */
window.tawkAPI = window.Tawk_API = window.Tawk_API || {}
window.tawkLoadStart = window.Tawk_LoadStart = new Date()

// console.log(window.Tawk_API)
// console.log(window.Tawk_LoadStart)
export default function liveChat () {
  let s1 = document.createElement('script')
  let s0 = document.getElementsByTagName('script')[0]
  s1.async = true
  s1.src = 'https://embed.tawk.to/5a0c877a198bd56b8c03b590/default'
  s1.charset = 'UTF-8'
  s1.setAttribute('crossorigin', '*')
  s0.parentNode.insertBefore(s1, s0)
}
