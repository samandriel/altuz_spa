// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import i18n from './locale'
import { store } from './store'
import bootstrap from 'bootstrap'
import liveChat from './config/livechat'
import VueAnalytics from 'vue-analytics'

document.liveChat = liveChat

Vue.config.productionTip = false

Vue.use(VueAnalytics, {
  id: 'UA-110476312-1',
  checkDuplicatedScript: true,
  autoTracking: {
    pageviewOnLoad: false
  },
  router
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  i18n,
  bootstrap,
  // popper,
  store,
  template: '<App/>',
  components: { App }
})
